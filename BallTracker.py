from Camera import Camera
import cv2
import numpy as np


class BallTracker:

    def __init__(self, first_camera: Camera, second_camera: Camera):
        self.first_camera = first_camera;
        self.second_camera = second_camera;

    def get_object_points(self):
        line_1 = self.first_camera.get_object_lines();
        line_2 = self.second_camera.get_object_lines();

        if line_1 is not None and line_2 is not None:
            pq_delta = np.array([
                [line_2[0, 0] - line_1[0, 0], line_2[0, 1] - line_1[0, 1], line_2[0, 2] - line_1[0, 2]],
                [line_2[1, 0], line_2[1, 1], line_2[1, 2]],  # S coefficients , for line_2
                [-line_1[1, 0], -line_1[1, 1], -line_1[1, 2]]  # T coefficients , for line_1
            ])

            test = np.dot(pq_delta[1], line_2[1])
            a = np.array([[np.dot(pq_delta[1], line_2[1]), np.dot(pq_delta[2], line_2[1])],
                          [np.dot(pq_delta[1], line_1[1]), np.dot(pq_delta[2], line_1[1])]])
            b = np.array([- np.dot(pq_delta[0], line_2[1]), - np.dot(pq_delta[0], line_1[1])])
            equation_result = np.linalg.solve(a, b)  # contains [ S, T] coefficient matrix

            q = equation_result[0] * line_2[1] + line_2[0]
            p = equation_result[1] * line_1[1] + line_1[0]

            final_result = (p + q) / 2

            print("final result ", final_result, " with distance ", np.linalg.norm(q - p))


if __name__ == "__main__":

    # logitech c920 camera_matrix and distortion_coef
    camera_matrix = np.array([[1.42509779e+03, 0.00000000e+00, 9.53624273e+02],
                               [0.00000000e+00, 1.43031041e+03, 5.32191265e+02],
                               [0.00000000e+00, 0.00000000e+00, 1.00000000e+00]]);
    distortion_coeffs = np.array([ 0.04378863, -0.21073288, -0.00109952,  0.00144811, 0.16525149])

    camera_1: Camera = Camera(camera_name="_1st_cam",
                              position_x=100, position_y=0, position_z=0,
                              forward_x=0, forward_y=0, forward_z=1, plane_rotation=0,
                              focal_length=900., horizontal_theta=67.93140962081645, vertical_theta=41.36709798048212,
                              video_capture=cv2.VideoCapture(0), camera_matrix=camera_matrix, distortion_coeffs=distortion_coeffs, kalman_enabled=False);

    camera_2: Camera = Camera(camera_name="_2st_cam",
                              position_x=0, position_y=0, position_z=0,
                              forward_x=0, forward_y=0, forward_z=1, plane_rotation=0,
                              focal_length=900.0, horizontal_theta=67.93140962081645, vertical_theta=41.36709798048212,
                              video_capture=cv2.VideoCapture(1), camera_matrix=camera_matrix, distortion_coeffs=distortion_coeffs, kalman_enabled=False);

    ball_tracker: BallTracker = BallTracker(camera_1, camera_2);
    while True:
        ball_tracker.get_object_points();
        key = cv2.waitKey(1) & 0xFF

        # if the 'q' key is pressed, stop the loop
        if key == ord("q"):
            break
