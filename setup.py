from setuptools import setup

setup(
    name='ball-tracking',
    version='0.1.0',
    install_requires=['cycler==0.10.0', 'filterpy==1.2.4', 'imutils==0.4.6', 'kiwisolver==1.0.1', 'matplotlib==2.2.2',
              'mpmath==1.0.0', 'numpy==1.14.2', 'opencv-python==3.4.0.12', 'pyparsing==2.2.0', 'python-dateutil==2.7.2',
              'pytz==2018.4', 'scipy==1.0.1', 'six==1.11.0', 'sympy==1.1.1', 'pyquaternion=0.9.2'],
    url='',
    license='',
    author='hosein bakhtiari',
    author_email='h.bakhtiary.z@gmail.com',
    description=''
)
