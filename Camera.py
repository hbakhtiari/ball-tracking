from cv2 import VideoCapture
import cv2
from collections import deque
import numpy as np
from filterpy.kalman import KalmanFilter
from pyquaternion import Quaternion
import math


class Camera:
    max_len = 50;
    blue_lower = (0, 0, 0);
    blue_upper = (255, 255, 100);

    def set_forward(self, forward_x, forward_y, forward_z):
        self.forward = np.array([forward_x, forward_y, forward_z]);
        return self.forward

    def set_plane_rotation(self, plane_rotation):
        self.plane_rotation = plane_rotation
        return self.plane_rotation

    def set_position(self, position_x, position_y, position_z):
        self.position = np.array([position_x, position_y, position_z])
        return self.position

    def apply_camera_updates(self):

        # update forward to a unit vector
        self.forward = self.forward / np.linalg.norm(self.forward)

        # update frame center
        self.frame_center_vector = self.focal_length * self.forward

        # finding right vector without applying plane_rotation
        if self.forward[2] == 0:
            self.right = np.array([self.forward[1], -self.forward[0], 0])
        else:
            self.right = np.array([self.forward[2], 0, self.forward[0]])

        # applying plane rotation on right vector
        if self.plane_rotation is not 0:
            plane_rotation_quaternion: Quaternion = Quaternion(axis=self.forward,
                                                               angle=math.radians(self.plane_rotation))
            self.right = plane_rotation_quaternion.rotate(self.right)

        # update right to a unit vector
        self.right = self.right / np.linalg.norm(self.right)

        # finding top vector
        self.top = np.cross(self.forward, self.right)

        # update top o a unit vector
        self.top = self.top / np.linalg.norm(self.top)

    def __init__(self, camera_name, position_x, position_y, position_z, forward_x, forward_y, forward_z, plane_rotation,
                 focal_length, horizontal_theta, vertical_theta, video_capture: VideoCapture, camera_matrix,
                 distortion_coeffs, kalman_enabled: bool):

        self.camera_name = camera_name
        self.focal_length = focal_length
        self.horizontal_theta = horizontal_theta
        self.vertical_theta = vertical_theta
        self.forward = self.set_forward(forward_x, forward_y, forward_z)
        self.plane_rotation = self.set_plane_rotation(plane_rotation)
        self.position = self.set_position(position_x, position_y, position_z)
        self.camera_matrix = camera_matrix
        self.distortion_coeffs = distortion_coeffs

        self.right = None
        self.top = None
        self.frame_center_vector = None
        self.apply_camera_updates()

        self.video_capture = video_capture;

        self.__pts__ = deque(maxlen=Camera.max_len);

        self.kalmanFilter = KalmanFilter(dim_x=4, dim_z=2, compute_log_likelihood=False);

        self.kalman_enabled = kalman_enabled;

        dt = 1;
        self.kalmanFilter.F = np.mat([
            [1, dt, 0, 0],
            [0, 1, 0, 0],
            [0, 0, 1, dt],
            [0, 0, 0, 1]
        ]);

        self.kalmanFilter.H = np.mat([
            [1, 0, 0, 0],
            [0, 0, 1, 0]
        ]);

        self.kalmanFilter.Q *= 4.;
        self.kalmanFilter.R = np.mat([
            [20000, 0],
            [0, 20000]
        ]);

        self.kalmanFilter.P *= 100.;

        # initialize kalman filter x parameter
        (grabbed, frame) = self.video_capture.read()

        # if we are viewing a video and we did not grab a frame,
        # then we have reached the end of the video
        if not grabbed:
            return None

        self.width = self.video_capture.get(cv2.CAP_PROP_FRAME_WIDTH)
        self.height = self.video_capture.get(cv2.CAP_PROP_FRAME_HEIGHT)
        center = self.__get_points(frame);

        if center is not None:
            self.kalmanFilter.x = np.mat([center[0], center[1], 0, 0]).T;
        else:
            self.kalmanFilter.x = np.mat([0, 0, 0, 0]).T;

    def __get_points(self, frame):

        # resize the frame, blur it, and convert it to the HSV
        # color space
        # frame = imutils.resize(frame, width=600)
        # blurred = cv2.GaussianBlur(frame, (11, 11), 0)

        newcameramtx, roi = cv2.getOptimalNewCameraMatrix(self.camera_matrix, self.distortion_coeffs,
                                                          (int(self.width), int(self.height)), 1,
                                                          (int(self.width), int(self.height)))
        dst = cv2.undistort(frame, self.camera_matrix, self.distortion_coeffs, None)
        lab = cv2.cvtColor(dst, cv2.COLOR_BGR2LAB)

        # construct a mask for the color "green", then perform
        # a series of dilations and erosions to remove any small
        # blobs left in the mask
        mask = cv2.inRange(lab, Camera.blue_lower, Camera.blue_upper)
        mask = cv2.erode(mask, None, iterations=2)
        mask = cv2.dilate(mask, None, iterations=2)

        # find contours in the mask and initialize the current
        # (x, y) center of the ball
        cnts = cv2.findContours(mask.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)[-2]
        center = None

        # only proceed if at least one contour was found
        if len(cnts) > 0:
            # find the largest contour in the mask, then use
            # it to compute the minimum enclosing circle and
            # centroid
            c = max(cnts, key=cv2.contourArea)
            ((x, y), radius) = cv2.minEnclosingCircle(c)
            M = cv2.moments(c)
            center = (int(M["m10"] / M["m00"]), int(M["m01"] / M["m00"]))

            # only proceed if the radius meets a minimum size
            if radius > 10:
                # draw the circle and centroid on the frame,
                # then update the list of tracked points
                cv2.circle(frame, (int(x), int(y)), int(radius),
                           (0, 255, 255), 2)
                # cv2.circle(frame, center, 5, (0, 0, 255), -1)

        return center

    def get_object_lines(self):
        results = None;

        (grabbed, frame) = self.video_capture.read()

        # if we are viewing a video and we did not grab a frame,
        # then we have reached the end of the video
        if not grabbed:
            return None

        centerMeasurement = self.__get_points(frame=frame);

        self.kalmanFilter.predict();

        if centerMeasurement is not None:
            self.kalmanFilter.update(np.mat([centerMeasurement[0], centerMeasurement[1]]));
        else:
            self.kalmanFilter.update(np.mat([0, 0]));

        if self.kalman_enabled:
            cv2.circle(frame, (int(self.kalmanFilter.x[0, 0]), int(self.kalmanFilter.x[2, 0])), 5, (0, 0, 255), -1)
        elif centerMeasurement is not None:
            cv2.circle(frame, (int(centerMeasurement[0]), int(centerMeasurement[1])), 5, (0, 0, 255), -1)
        else:
            cv2.circle(frame, (int(0), int(0)), 5, (0, 0, 255), -1)

        if centerMeasurement is not None:
            # points = np.array(centerMeasurement)
            # undistorted_points = cv2.undistortPoints(points, self.camera_matrix, self.distortion_coeffs)
            # print("centerMeasurement is ", centerMeasurement)
            # print("undistorted measurment is ", undistorted_points)
            # horizontal_angle = math.radians(self.horizontal_theta * (centerMeasurement[0] / self.width - 1/2))
            # vertical_angle = math.radians(self.vertical_theta * (centerMeasurement[1] / self.height - 1/2))
            horizontal_correction_scale = 1.42509779e+03
            vertical_correction_scale = 1.43031041e+00
            horizontal_angle = math.atan(math.tan(math.radians(self.horizontal_theta / 2)) * (
                    centerMeasurement[0] / self.width - 1 / 2) ) * horizontal_correction_scale
            vertical_angle = math.atan(
                math.tan(math.radians(self.vertical_theta / 2)) * (
                            centerMeasurement[1] / self.height - 1 / 2) ) * vertical_correction_scale
            final_quaternion = Quaternion(axis=self.right, angle=vertical_angle) * Quaternion(
                axis=self.top, angle=horizontal_angle)
            object_3d_vector = final_quaternion.rotate(self.frame_center_vector)
            # print("frame_center ", self.frame_center_vector)
            # print("forward ", self.forward)
            # print("right ", self.right)
            # print("top ", self.top)
            point_on_image_plane = object_3d_vector + self.position
            # print(point_on_image_plane)
            # if self.camera_name is "_1st_cam":
            #     print("Camera ", self.camera_name, " horizontal_angle=", horizontal_angle, " vertical_angle=", vertical_angle," object_3d_vector", object_3d_vector)
            results = np.array([self.position, object_3d_vector])
            # print(self.forward, self.right, self.frame_center_vector)
        # self.__pts__.appendleft(center);

        # # loop over the set of tracked points
        # for i in range(1, len(self.__pts__)):
        #     # if either of the tracked points are None, ignore
        #     # them
        #     if self.__pts__[i - 1] is None or self.__pts__[i] is None:
        #         continue
        #
        #     # otherwise, compute the thickness of the line and
        #     # draw the connecting lines
        #     thickness = int(np.sqrt(Camera.max_len / float(i + 1)) * 2.5)
        #     cv2.line(frame, self.__pts__[i - 1], self.__pts__[i], (0, 0, 255), thickness)

        # show the frame to our screen
        cv2.imshow("Frame" + self.camera_name, frame)

        return results;
